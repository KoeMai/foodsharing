# this must come before the variables section,
# so variables specified there can override defaults specified in the template
include:
  - template: Dependency-Scanning.gitlab-ci.yml

# to make the previous template work with kanthaus-runner, which expects these tags
dependency_scanning:
  tags:
    - saas-linux-medium-amd64

variables:
  # https://docs.docker.com/engine/userguide/storagedriver/selectadriver/
  # https://gitlab.com/gitlab-org/gitlab-ce/issues/19971
  DOCKER_DRIVER: overlay

stages:
  - outdated
  - build
  - test
  - deploy

.defaults: &defaults
  image: registry.gitlab.com/foodsharing-dev/images/ci/php:latest
  tags:
    - saas-linux-medium-amd64


.ssh: &ssh
  before_script:
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$DEPLOY_SSH_KEY")
    - mkdir -p ~/.ssh
    # For Docker builds disable host key checking. Be aware that by adding that
    # you are suspectible to man-in-the-middle attacks.
    # WARNING: Use this only with the Docker executor, if you use it with shell
    # you will overwrite your user's SSH config.
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

.deploy: &deploy
  <<: *defaults
  <<: *ssh
  stage: deploy
  tags:
    - non-shared
    - foodsharing # run it on our foodsharing ci server
  dependencies:
    - build:frontend
    - build:backend
    - build:websocket
    - build:docs
  script:
    - chmod -R 750 ./
    - (cd deployer && composer install)
    - deployer/vendor/bin/dep deploy $CI_ENVIRONMENT_NAME --revision $CI_COMMIT_SHA
    - ./scripts/deploy-websocket-restart
    - ./scripts/deploy-notify-slack

outdated:
  <<: *defaults
  stage: outdated
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "outdated"
      when: always
  tags:
    - non-shared
    - foodsharing
  script:
    # unfortunately "composer outdated -D" only works if you have installed the dependencies :/
    - composer install --verbose --prefer-dist --no-progress --no-interaction --classmap-authoritative --no-scripts --ignore-platform-reqs
    - ./scripts/ci-notify-outdated
  cache:
    key: outdated:cache
    paths:
      - vendor

build:websocket:
  <<: *defaults
  stage: build
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/node:latest
  script:
    - (cd websocket && yarn && yarn lint)
  cache:
    key: websocket:build:cache
    paths:
      - websocket/node_modules
  artifacts:
    expire_in: 1 week
    paths:
      - websocket/node_modules

build:frontend:
  <<: *defaults
  stage: build
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/node:latest
  script:
    - (cd client && yarn && yarn lint && yarn build)
  cache:
    key: frontend:build:cache
    paths:
      - assets
      - client/node_modules
  artifacts:
    expire_in: 1 week
    paths:
      - assets
      - sw.js

build:backend:
  <<: *defaults
  stage: build
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/php:latest
  script:
    - ./scripts/ci-backend-build
  cache:
    key: backend:build:cache
    paths:
      - vendor
      - .php-cs-fixer.php.cache
  artifacts:
    expire_in: 1 week
    paths:
      - assets
      - vendor

build:docs:
  <<: *defaults
  stage: build
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/docs:latest
  variables:
    MYSQL_ROOT_PASSWORD: root
    MYSQL_INITDB_SKIP_TZINFO: 1
  services:
    - name: registry.gitlab.com/foodsharing-dev/images/mariadb:latest
      alias: db
  script:
    - ./scripts/ci-docs-build
  artifacts:
    expire_in: 1 week
    paths:
      - docs

test:backend-code:
  <<: *defaults
  stage: test
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/php:latest
  variables:
    MYSQL_ROOT_PASSWORD: root
    MYSQL_INITDB_SKIP_TZINFO: 1
    TZ: Europe/Berlin
    FF_NETWORK_PER_BUILD: 1
    REDIS_HOST: redis
    FS_ENV: test
    XDEBUG_MODE: coverage
  coverage: '/^\s*Lines:\s*(\d+\.\d+)\%/'
  services:
    # Note: Gitlab runner will mark a lot of service healthchecks as failed.
    # This is a bug that will partly be addressed with the upcoming release of the gitlab-runner.
    # It is also due to our configuration, e.g. the websocket container not exposing ports but listed as a service.
    # Unfortunately, health check cannot be disabled.
    - name: registry.gitlab.com/foodsharing-dev/images/redis:latest
      alias: redis
    - name: registry.gitlab.com/foodsharing-dev/images/mariadb:latest
      alias: db
    - name: registry.gitlab.com/foodsharing-dev/images/selenium:latest
      alias: selenium
    - name: registry.gitlab.com/foodsharing-dev/images/maildev:latest
      alias: maildev
      command: ["bin/maildev",  "--web", "1080", "--smtp", "1025"]
    - name: registry.gitlab.com/foodsharing-dev/images/node:latest
      alias: websocket
      command: ["/bin/sh", "-c", 'rm $CI_PROJECT_DIR/code_updated; while [ ! -e $CI_PROJECT_DIR/code_updated ]; do sleep 1; done; cd /builds/foodsharing-dev/foodsharing/websocket && yarn ts-node src/index.ts 0.0.0.0']
      # websocket service runs with code that gets checked out later by the main build job.
      # We use a file to synchronize that, so start websocket service only when that file has been recreated.
    - name: registry.gitlab.com/foodsharing-dev/images/ci/nginx:latest
      alias: nginx
    - name: registry.gitlab.com/foodsharing-dev/images/influxdb:latest
      alias: influxdb
  dependencies:
    - build:frontend
    - build:backend
    - build:websocket
    - test:backend-acceptance
  script:
    - touch $CI_PROJECT_DIR/code_updated # Create file to notify websocket container that code is up to date
    - ./scripts/ci-backend-test-code
  cache:
    key: backend:build:cache
    paths:
      - vendor
      - .php-cs-fixer.php.cache
  artifacts:
    expire_in: 1 month
    when: always
    paths:
      - tests/_output
    reports: # make the report available in Gitlab UI. see https://docs.gitlab.com/ee/ci/unit_test_reports.html
      junit: tests/_output/report-code.xml
      coverage_report:
        coverage_format: cobertura
        path: tests/_output/code-cobertura.xml

test:backend-acceptance:
  <<: *defaults
  stage: test
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  tags:
    - foodsharing
    - shared
  image: registry.gitlab.com/foodsharing-dev/images/php:latest
  variables:
    MYSQL_ROOT_PASSWORD: root
    MYSQL_INITDB_SKIP_TZINFO: 1
    TZ: Europe/Berlin
    FF_NETWORK_PER_BUILD: 1
    REDIS_HOST: redis
    FS_ENV: test
  services:
    # Note: Gitlab runner will mark a lot of service healthchecks as failed.
    # This is a bug that will partly be addressed with the upcoming release of the gitlab-runner.
    # It is also due to our configuration, e.g. the websocket container not exposing ports but listed as a service.
    # Unfortunately, health check cannot be disabled.
    - name: registry.gitlab.com/foodsharing-dev/images/redis:latest
      alias: redis
    - name: registry.gitlab.com/foodsharing-dev/images/mariadb:latest
      alias: db
    - name: registry.gitlab.com/foodsharing-dev/images/selenium:latest
      alias: selenium
    - name: registry.gitlab.com/foodsharing-dev/images/maildev:latest
      alias: maildev
      command: ["bin/maildev",  "--web", "1080", "--smtp", "1025"]
    - name: registry.gitlab.com/foodsharing-dev/images/node:latest
      alias: websocket
      command: ["/bin/sh", "-c", 'rm $CI_PROJECT_DIR/code_updated; while [ ! -e $CI_PROJECT_DIR/code_updated ]; do sleep 1; done; cd /builds/foodsharing-dev/foodsharing/websocket && yarn ts-node src/index.ts 0.0.0.0']
      # websocket service runs with code that gets checked out later by the main build job.
      # We use a file to synchronize that, so start websocket service only when that file has been recreated.
    - name: registry.gitlab.com/foodsharing-dev/images/ci/nginx:latest
      alias: nginx
    - name: registry.gitlab.com/foodsharing-dev/images/influxdb:latest
      alias: influxdb
  dependencies:
    - build:frontend
    - build:backend
    - build:websocket
  script:
    - touch $CI_PROJECT_DIR/code_updated # Create file to notify websocket container that code is up to date
    - ./scripts/ci-backend-test-acceptance
  cache:
    key: backend:build:cache
    paths:
      - vendor
      - .php-cs-fixer.php.cache
  artifacts:
    expire_in: 1 month
    when: always
    paths:
      - tests/_output
    reports: # make the report available in Gitlab UI. see https://docs.gitlab.com/ee/ci/unit_test_reports.html
      junit: tests/_output/report-*.xml

test:frontend:
  <<: *defaults
  stage: test
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/node:latest
  variables:
    REDIS_HOST: redis
  script:
    - (cd client && yarn && yarn test)
  cache:
    key: frontend:build:cache
    paths:
      - client/node_modules

test:websocket:
  <<: *defaults
  stage: test
  interruptible: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'master' || $CI_COMMIT_BRANCH == 'production'
      changes:
        - 'websocket/**/*'
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - 'websocket/**/*'
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  image: registry.gitlab.com/foodsharing-dev/images/node:latest
  variables:
    REDIS_HOST: redis
  services:
    - name: registry.gitlab.com/foodsharing-dev/images/redis:latest
      alias: redis
  dependencies:
    - build:websocket
  script:
    - (cd websocket && yarn test)
  cache:
    key: websocket:build:cache
    paths:
      - websocket/node_modules

deploy:beta:
  <<: *deploy
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  environment:
    name: beta
    url: https://beta.foodsharing.de

deploy:production:
  <<: *deploy
  rules:
    - if: $CI_COMMIT_BRANCH == 'production'
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  environment:
    name: production
    url: https://foodsharing.de

deploy:docs:
  <<: *deploy
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  dependencies:
    - build:docs
  script:
    - rsync -avz --delete docs/build/ "deploy@devdocs.foodsharing.network:/var/www/devdocs/htdocs/"
  environment:
    name: docs
    url: https://devdocs.foodsharing.network
