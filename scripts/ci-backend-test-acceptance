#!/bin/bash

export FS_ENV=test

set -o errexit

dir=$(dirname "$0")

# shellcheck source=./__helping-functions.sh
source "$dir"/__helping-functions.sh
echo $SECONDS seconds elapsed


log-header "Preparing execution environment: Prepare PHP config, application & composer dev dependencies"
SECONDS=0
# configure path to application
# *https://forums.docker.com/t/sed-couldnt-open-temporary-file-xyz-permission-denied-when-using-virtiofs/125473/2
sed "s#/app#$CI_PROJECT_DIR#g" /usr/local/etc/php-fpm.d/fpm.conf > tmp; cat tmp > /usr/local/etc/php-fpm.d/fpm.conf; rm tmp
# let PHP-FPM run as root
# sed -i "s#www-data#root#g" /usr/local/etc/php-fpm.d/fpm.conf
# disable xdebug
# rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

bin/console foodsharing:setup
# also install dev packages
composer install --prefer-dist --no-progress --no-interaction --no-scripts --ignore-platform-reqs && composer outdated --direct
php-fpm -R 2> php_fpm_err.log &
echo $SECONDS seconds elapsed

log-header "Initializing database"
SECONDS=0
mysql -u root -proot -hdb --execute="drop database if exists foodsharing; create database foodsharing"
php run.php Mails queueWorker &
vendor/bin/phinx migrate
echo $SECONDS seconds elapsed

log-header "Waiting for assets to be generated ..."
while ! [ "$(ls -A assets)" ];
do
sleep 1;
echo -ne ".";
done
echo

log-header "Running tests"
failed=0
SECONDS=0

vendor/bin/codecept run acceptance --xml=report-acceptance-0.xml --html=report-acceptance-0.html || failed=1
echo $SECONDS seconds elapsed

if [ $failed -eq 0 ] # first run succeeded, we can finish early
then
  log-header "Done!"
  exit 0
fi

while [ $failed -le 3 ] # re-run failed tests 3 times
do
  log-header "Check for codeception errors"
  # check if codeception generated a report file that contains failed tests
  # Otherwise, codeception probably failed itself and the whole job should fail
  grep -E '<error|<failure' tests/_output/report-acceptance-$((failed - 1)).xml || \
    (echo "report.xml is incomplete, aborting" && false)

  log-header "Rerunning failed tests"
  SECONDS=0
  # create separate report files for each re-run so we don't overwrite the previous reports

  if vendor/bin/codecept run --xml=report-acceptance-$failed.xml --html=report-acceptance-$failed.html -g failed;
  then
    log-header "Done!"
    exit 0
  else
    ((failed++))
  fi
  echo $SECONDS seconds elapsed
done

# maximum tries reached
exit 1
